Feynman Diagrams
===

This repository provides feynman diagrams for the HWW analysis. Each file can be run directly with eg.
```
cd Higgs/HWW
lualatex VBF.tex
```

This generates the output `VBF.pdf`. The generation of feynman diagrams uses the *tikz*-based package *tikz-feynman*, which requires *lualatex*.

If you prefer to run *pdflatex*, you can try to use pure *tikz* to generate feynman diagrams. But some useful functions won't be available in this case.

If you need a docker image, try `texlive/texlive:latest`.
